from page_objects import PageObject, PageElement


class MainPage(PageObject):
    logout_link = PageElement(link_text="Logout")


    def check_page(self):
        return "Logged In" in self.w.title

    def click_logout(self,wpage):
        self.logout_link.click()
        return wpage.check_page()